<?php

Route::group(['middleware' => 'web', 'prefix' => 'actividades', 'namespace' => 'App\\Modules\Actividades\Http\Controllers'], function()
{
    Route::get('/', 'ActividadesController@index');
});
