<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\\Modules\Pagina\Http\Controllers'], function()
{
    Route::get('/', 'PaginaController@index');
});
