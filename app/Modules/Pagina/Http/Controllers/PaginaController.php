<?php

namespace App\Modules\Pagina\Http\Controllers;

use App\Modules\Pagina\Http\Controllers;

class PaginaController extends Controller
{
    public $titulo = 'Big Rhino';
    public $libreriasIniciales = [

	];
    public $css = [
        'ie9',
        'main',
        'noscript'
    ];

    public $js = [
        'skel.min',
        'util',
        'main'
    ];

    public function index()
    {
        return $this->view('pagina::index');
    }


}
