<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->
<head>
	@include('pagina::partials.header')
</head><!--/head-->

<body class="page-container-bg-solid">
<div class="row">
	<div class="col-md-12">
	    @yield('content-top')
		@yield('content')
	</div>
</div>
<div class="row">
    @include('pagina::partials.footer')
</div>
</body>
</html>
