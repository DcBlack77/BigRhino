@extends('pagina::layouts.master')

@section('content')
    <!-- Wrapper -->
        <div id="wrapper">

            <!-- Header -->
                <header id="header">
                    <div class="">
                        <img src="{{ url('public/img/logo/logo.png') }}" alt="Logo Box Big Rhino"width="45%">
                    </div>
                    <div class="content">
                        <div class="inner">
                            <h1>CrossFit Big Rhino</h1>
                            <p></p>
                        </div>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="#intro">Intro</a></li>
                            <li><a href="#box">Box</a></li>
                            <li><a href="#coachs">Coaches</a></li>
                            <li><a href="#horarios">Horarios</a></li>
                            <li><a href="#contact">Contacto</a></li>
                            <!--<li><a href="#elements">Elements</a></li>-->
                        </ul>
                    </nav>
                </header>

            <!-- Main -->
                <div id="main">

                    <!-- Intro -->
                        <article id="intro">
                            <h2 class="major">CrossFit</h2>
                            <span class="image main"><img src="images/intro_box.jpg" alt="Box Big Rhino" /></span>
                            <p>
                                El CrossFit se define como un sistema de entrenamiento de fuerza y acondicionamiento
                                basado en ejercicios funcionales constantemente variados realizados a una alta intensidad.
                            </p>
                        </article>

                    <!-- Work -->
                        <article id="box">
                            <h2 class="major">Box</h2>
                            <span class="image main">
                                <a href="images/pic02.jpg">
                                    <img src="images/pic02.jpg"/>
                                </a>
                            </span>
                            <p>
                                CrossFit Big Rhino es un centro oficial de CrossFit en Logroño, uno de los mejores boxes de la ciudad.
                                Disponemos del mejor equipamiento y los mejores coaches.
                            </p>
                            <span class="image main">
                                <a href="images/pic03.jpg">
                                    <img src="images/pic03.jpg"/>
                                </a>
                            </span>
                            <p>
                                Mas información del box
                            </p>
                            <span class="image main">
                                <a href="images/pic04.jpg">
                                    <img src="images/pic04.jpg"/>
                                </a>
                            </span>
                            <p>
                                Agregando mas información del sitio.
                            </p>
                        </article>

                    <!-- Coachs -->
                        <article id="coachs">
                            <h2 class="major">
                                RICHI
                            </h2>
                            <span class="image main left">
                                <!-- <iframe src="https://drive.google.com/open?id=0BxdyMyKs8MfAVXlpVjNYR3lkbExwdTFXUF9SSlo0bVl1d1Vn" frameborder="0"></iframe> -->
                                <video src="images/ricardo.mov #t=2" width="105%" controls autoplay loop muted></video>
                                <!-- <img src="images/riki.gif" alt="Ricardo Orio"> -->
                            </span>
                            <p>
                                El mayor de los 3 dueños del Box. Richi es el  cabeza de la familia Big Rhino.
                                Emprendedor, perfeccionista, detallista y  sobretodo le caracteriza su amabilidad hacia los demás.
                                Disfruta enseñando y  trata de sacar lo mejor de cada persona.
                                Paciente y con mucha energía es el  perfecto coach que hace que te sientas con fuerza
                                para alcanzar tus objetivos a  corto y largo plazo. Te animará, gritará, te hará reír y que des lo mejor
                                de  ti.
                                Nunca tiene un &ldquo;NO&rdquo; por respuesta.
                                <p>
                                    El CrossFit ahora es la mitad de su  corazón y lo vive cada día al máximo.
                                    Coach CF-L1 , curso de escalamiento en  proceso y practicando este deporte
                                    hace más de 3 años su estilo de vida ha  cambiado completamente creando un entorno siempre
                                    positivo a su alrededor. Su ejercicio  favorito es el THRUSTER (imposible pero cierto),
                                    y su comida favorita la pizza( la pide con  anchoa), nunca te va a dejar de sorprender.
                                </p>
                                <p>
                                    FRASE CÉLEBRE CUANDO SUFRE POR UNA TÉCNICA  INCORRECTA &ldquo; NO TE VENGAS ARRIBA&rdquo;
                                </p>
                                <p>
                                    &ldquo; El momento perfecto es hoy, las excusas  nunca valen&rdquo;
                                </p>
                            </p>
                            <h2 class="major">
                                Kiko
                            </h2>
                            <span class="image main right">
                                <video src="images/kiko.mov" width="100%" controls autoplay loop muted></video>
                                <!-- <img src="images/kiko.gif" alt="Francisco Sobrón"> -->
                            </span>
                            <p>
                                El pequeño de los 3 socios. Kiko Sobrón lleva tan sólo un año pero su progresión ha sido magnifica.
                            </p>
                            <p>
                                Dedicado al 100% para y por este deporte. Insistente,
                                energético y consigue todo lo que se propone. Entrenador CF-L1.
                            </p>
                            <p>
                                Pone todo su empeño e ilusión en los demás.
                            </p>
                            <p>
                                Te sorprenderá y hará que te sorprendas de ti mismo.
                                Te hará sentir cómodo y con ganas de no parar nunca.
                                Eso sí, a la hora de entrenar ya le puedes poner “reggeaton” y te hará los mejores tiempos
                                así que si te apetece entrenar a todo gas con música de este estilo,
                                son las clases perfectas para ti.
                            </p>
                            <p>
                                Sus clases son dinámicas y muy divertidas porque trata
                                que disfrutes en todo momento con este deporte como lo hace él.
                            </p>
                            <p>
                                Una vez que te enganches ya estás perdido,
                                perdido en el mejor estilo de vida que puedas encontrar,
                                el equilibrio perfecto del CrossFit.
                                “Conoce tus limitaciones para luego desafiarlas porque caerse en aceptable pero
                                renunciar no lo es”
                            </p>
                            <p>
                                Su HERO favorito es “JASON ”. GIRL favorito “AMANDA”. Su movimiento favorito es el “SNATCH”.
                            </p>
                            <h2 class="major">
                                Félix
                            </h2>
                            <span class="image main left">
                                <video src="images/felix.mov" width="100%" controls autoplay loop muted></video>
                                <!-- <img  src="images/felix.gif" width="45%" alt="Félix"> -->
                            </span>
                            <div class="right">
                                <p>
                                    El dueño que más tiempo lleva haciendo CrossFit. El mediano del equipo Big Rhino.
                                </p>
                                <p>
                                    A sus 24 años ha competido en distintas zonas de España y se ha subido al pódium en más de una ocasión.
                                    Federado y competidor en Halterofilia , entusiasta, alegre y muy técnico.
                                    Lleva toda la vida haciendo deporte y
                                    si está 2 días sin entrenar mejor que no te cruces con el ( tranquilos que no muerde).
                                </p>
                                <p>
                                    Su mejor amig@ se llama “BARRA”. Entrenador CF-L1.
                                    Empezó programándose a él mismo consiguiendo resultados positivos y siguió con su hermano, amigos etc.
                                    Conoce el campo de la nutrición relacionada con el deporte.
                                    Su dedicación hacia los demás con sus progresiones es lo que más le satisface
                                </p>
                                <p>
                                    Hace posible que cualquier persona que se lo proponga sobrepase sus límites.
                                    Si necesitas a un coach serio y muy técnico pero que a la vez esté en todo momento pendiente de ti es él.
                                    Nunca te va a sacar los ojos de encima y no te preocupes que la mejor calidad y técnica la conseguirás con él.
                                </p>
                                <p>
                                    Su HERO favorito es BATMAN, y relacionado con el CrossFit “MARK”. GIRL favorito “DIANE”
                                </p>
                                <p>
                                    “En el entrenamiento escucha tu cuerpo, en competición dile que se calle”. Rich Froning.
                                </p>
                            </div>

                        </article>

                    <!-- Horario -->
                        <article id="horarios" class="horarios">
                            <h2 class="major">
                                Horarios
                            </h2>
                                    <iframe src="HORARIO_BOX.htm" width="100%" height:"70rem" frameborder="100"></iframe>
                            <ul>
                                <li>WOD - Workout of Day </li>
                                <li>Endurce</li>
                                <li>Gymnastics</li>
                                <li>Halterofilia</li>
                                <li>CrossFit Football</li>
                                <li>OB - Open Box (Entrenamiento Libre)</li>
                            </ul>
                        </article>

                    <!-- Contact -->
                        <article id="contact">
                            <h2 class="major">Contact</h2>
                            <form method="post" action="#">
                                <div class="field half first">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" />
                                </div>
                                <div class="field half">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" id="email" />
                                </div>
                                <div class="field">
                                    <label for="message">Message</label>
                                    <textarea name="message" id="message" rows="4"></textarea>
                                </div>
                                <ul class="actions">
                                    <li><input type="submit" value="Send Message" class="special" /></li>
                                    <li><input type="reset" value="Reset" /></li>
                                </ul>
                            </form>
                            <ul class="icons">
                                <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                                <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                                <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                                <li><a href="#" class="icon fa-github"><span class="label">GitHub</span></a></li>
                            </ul>
                        </article>

                </div>

            <!-- Footer -->
                <footer id="footer">

                </footer>

        </div>

    <!-- BG -->
        <div id="bg"></div>
@stop
