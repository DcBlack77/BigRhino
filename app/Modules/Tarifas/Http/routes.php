<?php

Route::group(['middleware' => 'web', 'prefix' => 'tarifas', 'namespace' => 'App\\Modules\Tarifas\Http\Controllers'], function()
{
    Route::get('/', 'TarifasController@index');
});
