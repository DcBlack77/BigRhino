<?php

namespace App\Modules\Noticias\Models;

use App\Modules\Base\Models\Modelo;

class noticias_etiquetas extends Modelo
{
    protected $table = 'noticias_etiquetas';
    protected $fillable = ['noticias_id', 'etiquetas_id'];

    protected $primaryKey = null;
    public $incrementing = false;

    protected $hidden = ['created_at', 'updated_at'];
}
