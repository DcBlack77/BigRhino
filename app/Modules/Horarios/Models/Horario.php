<?php

namespace App\Modules\Horarios\Models;

use App\Modules\base\Models\Modelo;



class Horario extends modelo
{
    protected $table = 'horario';
    protected $fillable = ["ejercicio","dia","hora","detalles","published_at"];
    protected $campos = [
    'ejercicio' => [
        'type' => 'text',
        'label' => 'Ejercicio',
        'placeholder' => 'Ejercicio del Horario'
    ],
    'dia' => [
        'type' => 'text',
        'label' => 'Dia',
        'placeholder' => 'Dia del Horario'
    ],
    'hora' => [
        'type' => 'text',
        'label' => 'Hora',
        'placeholder' => 'Hora del Horario'
    ],
    'detalles' => [
        'type' => 'textarea',
        'label' => 'Detalles',
        'placeholder' => 'Detalles del Horario'
    ],
    'published_at' => [
        'type' => 'date',
        'label' => 'Publicar horario',
        'placeholder' => 'Fecha de publicaci\u00f3n del horario'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}