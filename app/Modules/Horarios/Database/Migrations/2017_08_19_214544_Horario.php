<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Horario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('actividades_id')->unsigned();
			$table->string('dia');
			$table->integer('hora');
			$table->timestamp('published_at')->nullable();

            $table->foreign('actividades_id')
				->references('id')->on('actividades')
                ->onDelete('cascade')->onUpdate('cascade');
                
			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario');
    }
}
