<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coachs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
      */
    public function up()
    {
        Schema::create('coachs', function (Blueprint $table) {
			$table->increments('id');
            $table->string('nombre', 250);
            $table->string('foto')->nullable();
            $table->string('alias');
			$table->integer('edad');
			$table->text('resumen');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coachs');
    }
}
