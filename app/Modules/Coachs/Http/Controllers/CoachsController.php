<?php

namespace App\Modules\Coachs\Http\Controllers;

//Controlador Padre
use App\Modules\Coachs\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Coachs\Http\Requests\CoachsRequest;

//Modelos
use App\Modules\Coachs\Models\Coachs;

class CoachsController extends Controller
{
    protected $titulo = 'Coachs';

    public $js = [
        'Coachs'
    ];
    
    public $css = [
        'Coachs'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('coachs::Coachs', [
            'Coachs' => new Coachs()
        ]);
    }

    public function nuevo()
    {
        $Coachs = new Coachs();
        return $this->view('coachs::Coachs', [
            'layouts' => 'base::layouts.popup',
            'Coachs' => $Coachs
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Coachs = Coachs::find($id);
        return $this->view('coachs::Coachs', [
            'layouts' => 'base::layouts.popup',
            'Coachs' => $Coachs
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Coachs = Coachs::withTrashed()->find($id);
        } else {
            $Coachs = Coachs::find($id);
        }

        if ($Coachs) {
            return array_merge($Coachs->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(CoachsRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Coachs = $id == 0 ? new Coachs() : Coachs::find($id);

            $Coachs->fill($request->all());
            $Coachs->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Coachs->id,
            'texto' => $Coachs->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Coachs::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Coachs::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Coachs::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Coachs::select([
            'id', 'nombre', 'edad', 'detalles', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}