<?php $__env->startSection('content'); ?>
    <div id="slider" class="slider">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo e(url('public/img/12.jpg')); ?>" alt="Banner principal">
            </div>
        </div>
    </div>
    <div id="unoticias" class="unoticias">
        <h2>Ultimas Noticias</h2>
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <h3>Twitter</h3>
                    <div class="twitter">
        				<h5 class="white-text" style="text-align:center;"><i class="fa fa-twitter blue-text" aria-hidden="true"></i>&nbsp;Twitter</h5>
        					 <a data-chrome="noheader nofooter transparent noborders"
        					 	data-tweet-limit="1"
        					 	class="twitter-timeline"
        					 	href="https://twitter.com/RRB1011FM"
        					 	data-widget-id="718264851123056642">
        					 </a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </div>
                    <div class="">
                        <h3>SoundCloud</h3>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <h3>Noticias</h3>
                <div class="noticias">
                    <img src="" alt="" class="col-md-4">
                    <div class="seccion-noticias col-md-8">
                        <section>
                            <h4>NOTICIA DE ULTIMO MOMENTO</h4>
                            <p>informacion de la noticia de ultimo momento</p>
                        </section>
                    </div>
                </div>
            </div>

        </div>
    </div>






    <div id="resena" class="resena">
        <h2>Información de la empresa</h2>
        <div class="row">
            <div class="col-md-3">
                <h3>Misión</h3>
            </div>
            <div class="col-md-3">
                <h3>Visión</h3>
            </div>
            <div class="col-md-3">
                <h3>Objetivos</h3>
            </div>
            <div class="col-md-3">
                <h3>Reseña</h3>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>