<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Programas']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Programas.',
        'columnas' => [
            'Titulo' => '33.333333333333',
		'Locutor' => '33.333333333333',
		'Url' => '33.333333333333'
        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

       <div class="row">                
            <div class="profile-sidebar col-md-3" style="margin-bottom: 35px;">
				<div class="portlet light profile-sidebar-portlet ">
					<div class="mt-element-overlay">
						<div class="row">
							<div class="col-md-12">
								<div class="mt-overlay-6">
									<img  id="foto" src="<?php echo e(url('public/img/usuarios/user.png')); ?>" class="img-responsive" alt="">
									<div class="mt-overlay">
										<h2> </h2>
										<p>
											<input id="upload" name="foto" type="file" />
											<a href="#" id="upload_link" class="mt-info uppercase btn default btn-outline">
												<i class="fa fa-camera"></i>
											</a>
										</p>
									</div>
									<h4 style="color:#fff;font-weight:bold;">Logo Programa</h4>
								</div>
							</div>
						</div>
					</div>
					<br />
				</div>
			</div> 
            <div class="col-md-9">
                <div class="panel panel-info ">
                    <div class="panel-heading">
                        <center>
                            <h3 class="panel-title">Programas</h3>
                        </center>
                    </div>
                    <div class="panel-body">
                        <?php echo e(Form::bsText('titulo', '', [
                            'label' => 'Nombre del Programa',
                            'placeholder' => 'Nombre del Programa',
                            'required' => 'required',
                            'class_cont' => 'col-md-12'
                        ])); ?>

                        <?php echo e(Form::bsText('locutor', '', [
                            'label' => 'Nombre del locutor',
                            'placeholder' => 'Nombre del locutor',
                            'required' => 'required',
                             'class_cont' => 'col-md-12'
                        ])); ?>


                    </div>
                </div>
            </div>
        </div>  
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>