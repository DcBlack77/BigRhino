<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Noticias']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Noticias.',
        'columnas' => [
            'titulo' => '40',
    		'resumen' => '60',

        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST']); ?>

            <?php echo e(Form::bsText('published_at', '', [
                'label' => 'Fecha',
                'placeholder' => 'Fecha de Publicaci&oacute;n'
            ])); ?>


            <div class="form-group col-xs-12">
                <label for="titulo">Titulo</label>
                <div id="titulo" class="form-control col-xs-12"></div>
            </div>

            <div class="form-group col-xs-12">
                <label for="resumen">Resumen</label>
                <div id="resumen" class="form-control col-xs-12"></div>
            </div>

            <div class="form-group col-xs-12">
                <label for="contenido_html">Contenido</label>
                <div id="contenido_html" class="col-xs-12"></div>
            </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>