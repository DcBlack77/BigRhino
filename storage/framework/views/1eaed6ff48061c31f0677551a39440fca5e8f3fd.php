<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $__env->make('pagina::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
    <div class="row" id="header">
        <div class="col-md-12 header">
            <div class="contenedor">
                <?php echo $__env->make('pagina::partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
    <div class="row" id="content">
        <div class="col-md-12">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
    <div class="row" id="footer">
        <div class="col-md-12">
            <div class="contenedor">
                <?php echo $__env->make('pagina::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
    <div id="foot">
        <div class="col-md-12">
            <?php echo $__env->make('pagina::partials.foot', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </div>
</body>
</html>
