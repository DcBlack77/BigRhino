<header>
    <div class="col-md-4 ">
        <div class="logo">
            <a href="#">
                <img src="<?php echo e(url('public/img/logos/logo_rrb.png')); ?>" alt="logo">
            </a>
        </div>
    </div>
    <div class="col-md-8 ">
        <div class="menu">
            <nav>
                <ul>
                    <li><a href="#">Inicio</a></li>
                    <li><a href="#">Nosotros</a></li>
                    <li><a href="#">Programas</a></li>
                    <li><a href="#unoticias">Articulos</a></li>
                    <li><a href="#">Multimedia</a></li>
                    <li><a href="#">Contactos</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
